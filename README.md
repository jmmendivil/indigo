# ⚡️ Indigo Challenges ⛹️‍♂️

Showcase project with [Indigo™](http://www.dsindigo.com) challenges exercises.

---
## 📚 Exercises

#### The Largest product in a series.

Find the greatest product of K consecutive digits in the N digit number.

#### The Largest palindrome product.

Find the largest palindrome made from the product of two 3-digit numbers which is less than N.

## 💼 Project structure

This project uses a simple configuration

```
.
├── README.md
├── package-lock.json
├── package.json
├── palindrome
│   ├── index.js
│   └── src
│       ├── methods.js
│       ├── methods.tests.js
│       └── read.js
└── product
    ├── index.js
    └── src
        ├── methods.js
        ├── methods.tests.js
        └── read.js

```

Each exercise _(palindrome, product)_ folder has:

- `index.js`: Main file to execute from
- `src/methods.js`: Methods used for the exercise
- `src/methods.test.js`: Methods test
- `src/read.js`: Read input interface for _CLI_ use

## 💻 Prerequisites

This project is written in _Javascript_, to run it you need to have [Node](https://nodejs.org/) _v9+_ installed.

Go to the official site to get instructions on how to install it in your SO, or use [nvm](https://github.com/creationix/nvm) _(recommended)_.

You will need [Git](https://git-scm.com/download) in order to clone this repository.

## 🏗 Installation

Clone this repository to your machine:

```
git clone https://jmmendivil@bitbucket.org/jmmendivil/indigo.git indigo
```

go to the clone repository folder:
```
cd indigo
```

and install required dependencies with:

```
npm install
```

## 🚀 Running the exercises

To run each exercise, use the `npm run <exercise>` command:

* Palindrome:

    ```
    npm run palindrome
    ```

* Product

    ```
    npm run product
    ```

## 🐾 Tests

You can run all the test in parallel with:

```
npm test
```

Or if you want it, run each project test:

* Palindrome:
    ```
    npm run test-palindrome
    ```

* Product:
    ```
    npm run test-product
    ```

## 👨‍💻 Author

* **Juan Mendivil** - [jmmendivil](https://github.com/jmmendivil)

## ✍️ Acknowledgments

This project uses:

* [Ava](https://github.com/avajs/ava) - Test runner

---
[![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)
