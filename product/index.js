/**
* Largest product in a serie
*
* Find the greatest product of K consecutive digits in the N digit number.
*/
const { getT, askNK, askInteger, resolveCase } = require('./src/methods')
const { read, readEnd } = require('./src/read')

// Go!
;(async function () {
  try {
    const T = await getT(read, 'Number of Test cases:')

    const casesInputArray = []
    for (let i = 0; i < T; i++) {
      const [ Ndigits, Knsecutive ] = await askNK(read, '2 integers (space separated): ')
      const integerInput = await askInteger(read, `${Ndigits} digits integer: `, Ndigits)
      casesInputArray.push({
        Ndigits,
        Knsecutive,
        integerInput
      })
    }

    const largestProducts = casesInputArray.map(resolveCase)
    console.log('>> Results:')
    console.log(largestProducts.join('\n'))
  } catch (e) {
    console.error('- Oups!\n', e)
  } finally {
    readEnd()
  }
})()
