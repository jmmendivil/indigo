import test from 'ava'
import {
  getT,
  askNK,
  askInteger,
  getGroups,
  getProduct,
  maxNumber,
  resolveCase
} from './methods'

const readMock = v => () => Promise.resolve(v)

// getT tests - constraints
test('getT : assign T', async t => {
  const rmock = readMock(5)
  const T = await getT(rmock)
  t.is(T, 5)
})
test('getT : throw on T < 1 constraint', async t => {
  const rmock = readMock(0)
  const error = await t.throwsAsync(getT(rmock))
  t.is(error.message, 'T constraint.')
})
test('getT : throw on T > 100 constraint', async t => {
  const rmock = readMock(101)
  const error = await t.throwsAsync(getT(rmock))
  t.is(error.message, 'T constraint.')
})

// askNK tests - constraints
test('askNK : assign N and K to Array', async t => {
  const rmock = readMock('10 5')
  const NK = await askNK(rmock)
  t.deepEqual(NK, [10, 5])
})
test('askNK : throw when K (9) <= 7 constraint', async t => {
  const rmock = readMock('10 9')
  const error = await t.throwsAsync(askNK(rmock))
  t.is(error.message, 'K constraint.')
})
test('askNK : throw when K (0) >= 1 constraint', async t => {
  const rmock = readMock('10 0')
  const error = await t.throwsAsync(askNK(rmock))
  t.is(error.message, 'K constraint.')
})
test('askNK : throw when N (5) >= K (7) constraint', async t => {
  const rmock = readMock('5 7')
  const error = await t.throwsAsync(askNK(rmock))
  t.is(error.message, 'N constraint.')
})
test('askNK : throw when N (1001) >= 1000 constraint', async t => {
  const rmock = readMock('1001 7')
  const error = await t.throwsAsync(askNK(rmock))
  t.is(error.message, 'N constraint.')
})

// askInteger tests - constraints
test('askInteger : assign string', async t => {
  const rmock = readMock('123456')
  const integerInput = await askInteger(rmock)
  t.is(integerInput, '123456')
})
test('askInteger : throw when integer (123) < N (5) digits', async t => {
  const rmock = readMock('123')
  const error = await t.throwsAsync(askInteger(rmock, null, 5))
  t.is(error.message, 'Needs more digits!')
})

// getGroups tests
test('getGroups : groups array', t => {
  let groups = getGroups('12345', 3)
  t.deepEqual(groups, ['123', '234', '345'])

  groups = getGroups('abcdefghijk', 8)
  t.deepEqual(groups, ['abcdefgh', 'bcdefghi', 'cdefghij', 'defghijk'])

  groups = getGroups('1234', 1)
  t.deepEqual(groups, ['1', '2', '3', '4'])
})

// getProduct tests
test('getProduct : resolve product', t => {
  let product = getProduct('12345')
  t.is(product, 120)

  product = getProduct('02468')
  t.is(product, 0)
})

// maxNumber tests
test('maxNumber : resolve max', t => {
  let max = maxNumber([1, 2, 3, 4, 5])
  t.is(max, 5)

  max = maxNumber([-3, -2, -1, 0])
  t.is(max, 0)
})

// resolveCase tests
test('resolveCase : resolve case', t => {
  let tcase = resolveCase({
    Ndigits: 10,
    Knsecutive: 5,
    integerInput: '3675356291'
  })
  t.is(tcase, 3150)

  tcase = resolveCase({
    Ndigits: 10,
    Knsecutive: 5,
    integerInput: '2709360626'
  })
  t.is(tcase, 0)
})
