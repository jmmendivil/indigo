/*
 * Largest product in a serie
 * METHODS
 */

/**
 * Ask the number of test cases
 *
 * @async
 * @func readT
 * @param {String} message - Ask message
 */
async function getT (read, message) {
  const T = await read(message)
  if (!(T >= 1 && T <= 100)) throw new Error('T constraint.')
  return T
}

/**
 * Ask N and K values
 *
 * @async
 * @func askNK
 * @param {Function} read - Read input function
 * @param {String} message - Ask message
 * @return {[Ndigits: Number, Knsecutive:Number]} - N and K values
 */
async function askNK (read, message) {
  const NK = await read(message)

  const NKsplit = NK.split(' ')
  const Ndigits = parseInt(NKsplit[0])
  const Knsecutive = parseInt(NKsplit[1])

  if (!(Knsecutive >= 1 && Knsecutive <= 7)) throw new Error('K constraint.')
  if (!(Ndigits >= Knsecutive && Ndigits <= 1000)) throw new Error('N constraint.')

  return [Ndigits, Knsecutive]
}

/**
 * Ask for N digits integer
 *
 * @async
 * @func askInteger
 * @param {Function} read - Read input function
 * @param {String} message - Ask message
 * @param {Number} Ndigits - N digits to test input
 * @return {Number} - Integer input as string
 */
async function askInteger (read, message, Ndigits) {
  const integerInput = await read(message)
  if (integerInput.length < Ndigits) throw new Error('Needs more digits!')
  return integerInput
}

/**
 * Generate series of N chars of length from submited string
 *
 * @func getGroups
 * @param {String} fullString - String to get items from
 * @param {Number} length - The length of chars per item
 * @return {String[]} - Generated groups of items
 */
function getGroups (fullString, length) {
  const groups = []
  const groupsTotal = fullString.length - length
  for (var i = 0; i <= groupsTotal; i++) {
    const stringGroup = fullString.slice(i, (length + i))
    groups.push(stringGroup)
  }
  return groups
}

/**
 * Get product from the numbers in a line
 *
 * @func getProduct
 * @param {String} line - String of numbers to multiply
 * @return {Number} - Final product of all numbers
 */
function getProduct (line) {
  return line.split('').reduce((c, p) => c * p)
}

/**
 * Get max number
 *
 * @func maxNumber
 * @param {Number[]} - Array of numbers to compare
 * @return {Number} Max number in the serie
 */
function maxNumber (numbers) {
  return Math.max.apply(null, numbers)
}

/**
 * Resolve "large product in a serie" case:
 * - Generate Ndigits items groups
 * - Get product of group
 * - Get largest product
 *
 * @func resolveCase
 * @param {Object} testCase - A test case with user inputs
 * @param {Number} testCase.Ndigits - Number of digits for the integer input
 * @param {Number} testCase.Knsecutive - Number of consecutive * digits for
 * each serie
 * @param {Number} testCase.integerInput - Integer input string with Ndigits
 * @return {Number} - Bigest product in the consecutive series
 */
function resolveCase ({ Ndigits, Knsecutive, integerInput }) {
  const groups = getGroups(integerInput, Knsecutive)
  const groupProducts = groups.map(getProduct)
  const maxProduct = maxNumber(groupProducts)
  return maxProduct
}

module.exports = {
  getT,
  askNK,
  getGroups,
  getProduct,
  maxNumber,
  askInteger,
  resolveCase
}
