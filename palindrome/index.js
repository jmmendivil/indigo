/*
 * Largest palindrome product
 *
 * Find the largest palindrome made from the product of two 3-digit
 * numbers which is less than N.
 */

const { getT, askInteger, resolveCase } = require('./src/methods')
const { read, readEnd } = require('./src/read')

// Do it!
;(async function () {
  try {
    const T = await getT(read, 'Number of Test cases:')

    const casesInputArray = []
    for (let i = 0; i < T; i++) {
      const integerInput = await askInteger(read, 'N digits integer (<1000000): ')
      casesInputArray.push(integerInput)
    }

    const palindromes = casesInputArray.map(resolveCase)
    console.log('>> Results:')
    console.log(palindromes.map(p => p.numpal).join('\n'))
  } catch (e) {
    console.error('- Oups!\n', e)
  } finally {
    readEnd()
  }
})()
