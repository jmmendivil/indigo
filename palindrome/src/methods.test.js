import test from 'ava'
import {
  getT,
  askInteger,
  isPalindrome,
  getFactors,
  resolveCase
} from './methods'

const readMock = v => () => Promise.resolve(v)

// getT tests - constraints
test('getT : assign T', async t => {
  const rmock = readMock(5)
  const T = await getT(rmock)
  t.is(T, 5)
})
test('getT : throw on T < 1 constraint', async t => {
  const rmock = readMock(0)
  const error = await t.throwsAsync(getT(rmock))
  t.is(error.message, 'T constraint.')
})
test('getT : throw on T > 100 constraint', async t => {
  const rmock = readMock(101)
  const error = await t.throwsAsync(getT(rmock))
  t.is(error.message, 'T constraint.')
})

// askInteger tests - constraints
test('askInteger : assign number', async t => {
  const rmock = readMock('123456')
  const integerInput = await askInteger(rmock)
  t.is(integerInput, 123456)
})
test('askInteger : throw when integer (1000001) > 1000000', async t => {
  const rmock = readMock('1000001')
  const error = await t.throwsAsync(askInteger(rmock, null, 5))
  t.is(error.message, 'N constraint.')
})

// isPalindrome tests
test('isPalindrome : test true', t => {
  let palindrome = isPalindrome('1023003201')
  t.true(palindrome)

  palindrome = isPalindrome('1221')
  t.true(palindrome)
})
test('isPalindrome : test false', t => {
  let palindrome = isPalindrome('1234')
  t.false(palindrome)

  palindrome = isPalindrome('0988123')
  t.false(palindrome)
})

// getFactors tests
test('getFactors : resolve factors', t => {
  let factors = getFactors(101101)
  t.deepEqual(factors, [143, 707])

  factors = getFactors(793397)
  t.deepEqual(factors, [869, 913])
})

// resolveCase test
test('resolveCase : resolve case', t => {
  let tcase = resolveCase(101110)
  t.deepEqual({
    numpal: 101101,
    factors: [143, 707]
  }, tcase)

  tcase = resolveCase(800000)
  t.deepEqual({
    numpal: 793397,
    factors: [869, 913]
  }, tcase)
})
