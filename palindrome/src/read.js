/**
 * Read line interface
 */
const rl = require('readline').createInterface({
  input: process.stdin,
  output: process.stdout
})

/**
 * Read input from stdin
 *
 * @func read
 * @param {String} message - The message to show on prompt
 * @return {Promise} - User input values
 */
function read (message) {
  return new Promise((resolve, reject) => {
    rl.question(message, (answer) => { resolve(answer) })
  })
}

/*
 * Close read line interface
 *
 * @finc readEnd
 */
function readEnd () {
  rl.close()
}

module.exports = {
  read,
  readEnd
}
