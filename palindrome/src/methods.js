/*
 * Largest palindrome
 * METHODS
 */

/**
 * Ask the number of test cases
 *
 * @async
 * @func readT
 * @param {String} message - Ask message
 */
async function getT (read, message) {
  const T = await read(message)
  if (!(T >= 1 && T <= 100)) throw new Error('T constraint.')
  return T
}

/**
 * Ask for N digits integer
 *
 * @async
 * @func askInteger
 * @param {Function} read - Read input function
 * @param {String} message - Ask message
 * @param {Number} Ndigits - N digits to test input
 * @return {Number} - Integer input as string
 */
async function askInteger (read, message) {
  let integerInput = await read(message)
  integerInput = parseInt(integerInput)
  if (integerInput < 1 ||
    integerInput >= 1000000 ||
    isNaN(integerInput)) throw new Error('N constraint.')
  return integerInput
}

/**
 * Resolves if palindrome
 *
 * @func isPalindrome
 * @param {Number} number - Number to test
 * @return {Boolean}
 */
function isPalindrome (number) {
  const nString = String(number)
  const rString = nString.split('').reverse().join('')
  return nString === rString
}

/**
 * Get posible 3-digits factors for the product
 *
 * @func getFactors
 * @param {Number} num - Number to find factors
 * @return {Number[]||null} - Returns array with used factors or null if no
 * found
 */
function getFactors (num) {
  for (let a = 100; a < 999; a++) {
    for (let b = 100; b < 999; b++) {
      if ((a * b) === num) return [a, b]
    }
  }
  return null
}

/**
 * Resolves current case
 *
 * @func resolveCase
 * @param {Number} number - Number to test case
 * @return {Object} - Resolved object
 * @return {Object.num} - Largest palindrome
 * @return {Object.factors} - Used factors for palindrome
 */
function resolveCase (number) {
  let numpal = number
  let factors = null
  let found = false
  while (!found) {
    let isPal = false
    while (!isPal) {
      isPal = isPalindrome(--numpal)
    }
    factors = getFactors(numpal)
    if (factors !== null) {
      found = true
    } else {
      isPal = false
    }
  }
  return { numpal, factors }
}

module.exports = {
  getT,
  isPalindrome,
  askInteger,
  getFactors,
  resolveCase
}
